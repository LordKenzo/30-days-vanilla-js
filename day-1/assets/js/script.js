document.addEventListener('keydown', keypressed);

function keypressed(e){
  console.log(e.key)
  var keyIndex = keyset.indexOf(e.key.toLowerCase());
  if(keyIndex < 0) return;
  var key = document.querySelector(`li[data-key=${e.key.toLowerCase()}]`);
  var audio = document.querySelector(`audio[data-key=${e.key.toLowerCase()}]`);
  if(!audio || !key) return;
  
  key.className += 'on-play';
  audio.currentTime = 0;
  audio.play();
}

var keyset = ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'];

var keys = document.querySelectorAll('li[data-key]');
keys.forEach(key => key.addEventListener('transitionend', function(e){
  e.target.className = '';
}));

/*var keyset = {
  a : 'A',
  s : 'S',
  d : 'D',
  f : 'F',
  g : 'G',
  h : 'H',
  j : 'J',
  k : 'K',
  l : 'L'
}*/