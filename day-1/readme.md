# Day 1 - Drum Kit

Creiamo una lista di tasti che l'utente potrà "suonare" premendo i tasti dalla "A" alla "L" (seconda fila della tastiera QWERTY).

Ogni tasto corrisponde ad un suono. I suoni verranno inseriri nel nuovo tag HTML5 <audio> e sfrutterò l'attributo src che individua il sorgente.
Ad ogni audio inseriremo un identificatore, sfruttando, anzichè l'attributo id, l'attributo data-* anche questo introdotto da HTML5 e utile per memorizzare meta dati da utilizzare nei nostri script JS e quindi rendere più semplice il codice:

```html
data-key=""
```

ottenendo qualcosa di simile a questo:

```
<audio src="assets/audio/boom.wav" data-key=""></audio>
```

e la stessa cosa la faccio ai tasti dell'ipotetica "batteria" (drum):

```html
<ul class="drum__key">
    <li data-key="">A</li>
    <li data-key="">S</li>
    <li data-key="">D</li>
    <li data-key="">F</li>
    <li data-key="">G</li>
    <li data-key="">H</li>
    <li data-key="">J</li>
    <li data-key="">K</li>
    <li data-key="">L</li>
  </ul>
```

Ovviamente dovrò inserire qualcosa all'interno di data-key. Ma cosa?
L'idea è di inserire il valore del codice che mi ritorna la pressione del tasto A,S,..L.

Il passo successivo è catturare la pressione del tasto. A chi posso associare l'evento "keydown"? A tuti quegli elementi che possono essere oggetti del "focus" come ad esempio l'input field, mentre non posso agganciarlo ad un div, in quanto elemento non direttamente selezionabile con focus.
Pertanto posso decidere di agganciare l'evento di keydown all'elemeno window o document:

```js
document.addEventListener('keydown', keypressed);

function keypressed(e){
  console.log(e.key);
}
```

Adesso, una volta individuato come catturare la pressione del tasto, posso notare che l'e.key mi è utile per aggiungere il meta-data mancante ai miei audio e li tag del dom, e scriverò proprio le lettere dell'alfabeto che rappresentano i miei tasti.

```html
<ul class="drum__key">
    <li data-key="a">A</li>
    <li data-key="s">S</li>
    <li data-key="d">D</li>
    <li data-key="f">F</li>
    <li data-key="g">G</li>
    <li data-key="h">H</li>
    <li data-key="j">J</li>
    <li data-key="k">K</li>
    <li data-key="l">L</li>
</ul>
...

  <audio src="./assets/audio/boom.wav" data-key="a"></audio>
  <audio src="./assets/audio/clap.wav" data-key="s"></audio>
  <audio src="./assets/audio/hihat.wav" data-key="d"></audio>
  <audio src="./assets/audio/kick.wav" data-key="f"></audio>
  ...
```

Definisco l'insieme dei tasti ammissibili:

```js
var keyset = ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'];
```

verifo che il tasto premuto faccia parte di questo insieme, se non lo fosse interrompo direttamente lo script con la return:

```js
var key = keyset.indexOf(e.key);
if(key < 0) return;
```

se il tasto premuto è ammissibile, selezionerò l'audio e lo suono con il play. Attenzione, se non mettessi il currentTime, premendo più volte, mi perderei i suoni (prova):

```js
var audio = document.querySelector(`audio[data-key=${e.key}]`)
audio.currentTime = 0;
audio.play();
```

Utilizzo il querySelecton per prelevare l'audio dell'e.key premuto.

Adesso devo trovare il modo di aggiungere e togliere una classe CSS per dare un effetto di pressione.
Per l'aggiunta mi basterà inserirla poco prima del play(), mentre per rimuovere la classe dell'effetto dovrò usare un event listener di "transionend".
Ma attenzione, l'elemento che scatena l'evento del keydown è associato al document (o altro) e quindi non posso agganciare la classe direttamente a questo elemento. Dovrò anche qui prelevare l'elemento in base al meta-data:

```js
var key = document.querySelector(`li[data-key=${e.key}]`);
...
key.className += 'on-play';
```

Per agganciare un evento di transitionend devo scorrere i miei tasti key e agganciare un event listener:

```js
var keys = document.querySelectorAll('li[data-key]');
keys.forEach(key => key.addEventListener('transitionend', function(e){
  e.target.className = '';
}));
```

ATTENZIONE: il keys di querySelectorAll non è propriamente un array, quindi non posso usare map, ma è una staticNodeList, per cui userò la forEach.

Lo script:

```js
document.addEventListener('keydown', keypressed);

function keypressed(e){
  
  var keyIndex = keyset.indexOf(e.key);
  if(keyIndex < 0) return;
  var key = document.querySelector(`li[data-key=${e.key}]`);
  var audio = document.querySelector(`audio[data-key=${e.key}]`);
  if(!audio || !key) return;
  
  key.className += 'on-play';
  audio.currentTime = 0;
  audio.play();
}

var keyset = ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'];

var keys = document.querySelectorAll('li[data-key]');
keys.forEach(key => key.addEventListener('transitionend', function(e){
  e.target.className = '';
}));
```